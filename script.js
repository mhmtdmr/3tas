import Oyun from "./models/oyun.js";
import { tasAlanlari,tasKoordinatlari } from "./enums.js";
import Tas from "./models/tas.js";


const baslaButon = document.getElementById("baslaButon");
baslaButon.addEventListener("click",Oyun.Basla);


tasAlanlari.forEach((tasAlani,i) => {
    let element = document.getElementById(`tasAlani${tasAlani}`);
    element.addEventListener("click",() => {
        let [x,y] = tasKoordinatlari[i];
        if (Oyun.aktifOyuncu.tasSayisi<3)
        { 
            Oyun.TasEkle(x,y);
        } else {
            if (Tas.seciliTas)
                {
                    Tas.moveTas(x,y);
                }
        }

    }); 
});