const locations = new Map([
    [0,-10],
    [1,190],
    [2,390]
]);

const tasAlanlari = [1,2,3,4,5,6,7,8,9];
const tasKoordinatlari = [
    [0,0],[1,0],[2,0],
    [0,1],[1,1],[2,1],
    [0,2],[1,2],[2,2]
];
export {locations,tasAlanlari,tasKoordinatlari};