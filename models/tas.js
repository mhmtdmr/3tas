import { locations } from "../enums.js";
import Oyun from "./oyun.js";


class Tas {
    static seciliTas = undefined;
    static tasSayac = 0;

    constructor(x,y,renk) {
        this.x = x;
        this.y = y;
        this.left = locations.get(x) + "px";
        this.top = locations.get(y) + "px";
        this.renk = renk;
        this.htmlVal = this.getHtml();
        Tas.tasSayac += 1;
    }

    /**
     * Taşları alan üzerinde taşıma işlemini yapar
     * @param {*} x 
     * @param {*} y 
     */
    static moveTas = (x,y) => {
        // Kontrol
        let xFark = x - Tas.seciliTas.x;
        let yFark = y - Tas.seciliTas.y;

        // Oyun kuralı gereği tek bir eksende 1 adım atılabilir.
        if( Math.abs(xFark + yFark) == 1) {

            // Oyun dizisindeki eski konumu resetle
            Oyun.oyunDizisi[Tas.seciliTas.y][Tas.seciliTas.x] = "0";

            Tas.seciliTas.x = x;
            Tas.seciliTas.y = y;
            Tas.seciliTas.left = locations.get(x) + "px";
            Tas.seciliTas.top = locations.get(y) + "px";
            Tas.seciliTas.htmlVal.style.top = Tas.seciliTas.top;              // soldan ve üstten uzaklığını hesaplayıp ekledik.
            Tas.seciliTas.htmlVal.style.left = Tas.seciliTas.left;
    
            // Taş seçimini kaldı.
            Tas.seciliTas.htmlVal.classList.remove("seciliTas");
            Tas.seciliTas = undefined;

            // Oyun dizisindeki yeni konumu işaretle
            Oyun.oyunDizisi[y][x] = Oyun.aktifOyuncu.harf;


            // Kazanma durumunu kontrol et!
            Oyun.OyuncuDegistir();
            Oyun.KazanmaKontrol();

        }else {
            alert("Lütfen çizgileri takip ederek 1 adım ileri taşıyınız!");
        }

    }

    /**
     * Taş öğesini temsilen yeni bir div oluşturarak döndürür.
     */
    getHtml = () => {
        const element = document.createElement("div"); // yeni bir dib oluştur.
        element.classList.add("tas");   // class="tas" clasını ekle.
        element.style.backgroundColor = this.renk; // arka planını kullanıcının renginden al.
        element.style.top = this.top;              // soldan ve üstten uzaklığını hesaplayıp ekledik.
        element.style.left = this.left;
        element.addEventListener("click",() => {

            // TODO: Oyun.aktifOyuncu static yapıldığı için bu bilgiye oradan erişilebilir.
            let aktifOyuncuRenk = localStorage.getItem("aktifOyuncuRenk");

            // Taşların tamamı dizildi ise ve kendi taşına tıkladıysa seçsin
            if (Tas.tasSayac == 6 && this.renk == aktifOyuncuRenk) {
                
                // Bir önceki seçili taş varsa ondan seçimi kaldır.
                if (Tas.seciliTas)
                    Tas.seciliTas.htmlVal.classList.remove("seciliTas");

                Tas.seciliTas = this;
                element.classList.add("seciliTas");
            }else {
                alert("Lütfen tüm taşları diziniz! Tüm taşlar dizili ise kendi taşınızı seçiniz!!!");
            }

        });
        return element;
    }
}

export default Tas;