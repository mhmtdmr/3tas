import Tas from "./tas.js";
import Oyuncu from "./oyuncu.js"

class Oyun {
    static oyunDizisi = [
        ["0","0","0"],
        ["0","0","0"],
        ["0","0","0"],
    ];
    static oyuncu1 = undefined;
    static oyuncu2 = undefined;
    static aktifOyuncu = undefined;
    static kayitEkrani = document.getElementById("kayit");
    static oyunAlani = document.getElementById("oyun");
    static ustDuzlem = document.getElementById("ustduzlem");
    static oyuncu1Span = document.getElementById("oyuncu1");
    static oyuncu2Span = document.getElementById("oyuncu2");

    static Basla = () => {
        // TODO: Boş değer kontrolü yapılacak.
        const kull1 = document.getElementById("kullanici1").value;
        const kull2 = document.getElementById("kullanici2").value;
        const renk1 = document.getElementById("renk1").value;
        const renk2 = document.getElementById("renk2").value;

        Oyun.oyuncu1 = new Oyuncu(kull1,renk1,"1");
        Oyun.oyuncu2 = new Oyuncu(kull2,renk2,"2");
        
        Oyun.oyuncu1Span.innerText = Oyun.oyuncu1.ad;
        Oyun.oyuncu2Span.innerText = Oyun.oyuncu2.ad;

        Oyun.kayitEkrani.classList.remove("flex-col");
        Oyun.kayitEkrani.classList.add("d-none");
        Oyun.oyunAlani.classList.remove("d-none");
        Oyun.oyunAlani.classList.add("flex-col");

        // Varsayılan olarak oyuna 1. oyuncu başlar.
        Oyun.aktifOyuncu = Oyun.oyuncu1;
    }

    static OyuncuDegistir = () => {
        // Aktif oyuncuyu değiştir
        if (Oyun.aktifOyuncu === Oyun.oyuncu1) {
            Oyun.aktifOyuncu = Oyun.oyuncu2;
        } else {
            Oyun.aktifOyuncu = Oyun.oyuncu1;
        }
        
        // Renkli kenarlık işlemleri
        Oyun.oyuncu1Span.classList.toggle("aktifOyuncu");
        Oyun.oyuncu2Span.classList.toggle("aktifOyuncu");
    
        // Aktif oyuncunun rengini localStorage'e kaydet
        localStorage.setItem("aktifOyuncuRenk", Oyun.aktifOyuncu.renk);
    }
    

    /**
     * Oyun başlarken taşların dizilmesini sağlar.
     * @param {*} x 
     * @param {*} y 
     */
    static TasEkle = (x,y) => {
        let tas = new Tas(x,y,Oyun.aktifOyuncu.renk);
        Oyun.ustDuzlem.appendChild(tas.htmlVal);
        Oyun.aktifOyuncu.tasSayisi += 1;
        Oyun.oyunDizisi[y][x] = Oyun.aktifOyuncu.harf;
        Oyun.OyuncuDegistir();
    }


    /**
     * Taş taşıma hamlelerinden sonra kazanma durumu oluşu oluşmadığını kontrol eder.
     * @returns 
     */
    static KazanmaKontrol() {
        const oyunDizisi = Oyun.oyunDizisi; // Oyun dizisini kısaltmak için bir değişkene atadık
    
        // Dikey ve yatay kontrol fonksiyonları
        function kontrolEt(a, b, c) {
            if (a === b && b === c && a != "0" ) {
                return true; // Eğer üç değer birbirine eşitse true döner
            }
            return false; // Eşit değilse false döner
        }
    
        // Yatay kontrol
        for (let x = 0; x < oyunDizisi.length; x++) {
            if (kontrolEt(oyunDizisi[x][0], oyunDizisi[x][1], oyunDizisi[x][2])) {
                alert(`Tebrikler ${Oyun.aktifOyuncu.ad} kazandınız!`);
                return; // Kazandıysa fonksiyondan çık
            }
        }
    
        // Dikey kontrol
        for (let y = 0; y < oyunDizisi.length; y++) {
            if (kontrolEt(oyunDizisi[0][y], oyunDizisi[1][y], oyunDizisi[2][y])) {
                alert(`Tebrikler ${Oyun.aktifOyuncu.ad} kazandınız!`);
                return; // Kazandıysa fonksiyondan çık
            }
        }
    }

    
}

export default Oyun;