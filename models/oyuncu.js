class Oyuncu {
    constructor(ad,renk,harf) {
        this.ad = ad;
        this.renk = renk;
        this.harf = harf;
        this.tasSayisi = 0;
    };

    renkDegistir(yeniRenk) {
        this.renk = yeniRenk;
    }
}
export default Oyuncu;